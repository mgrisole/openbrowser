import sublime, sublime_plugin, webbrowser, re

def toggleConsole():
    sublime.active_window().run_command("show_panel", {"panel": "console", "toggle": True})
    return;

def searchClient( client, testMode ):

    settings = sublime.load_settings('OpenBrowser.sublime-settings')    
    urls = settings.get( 'devaticsClients' )

    if client in urls:
        if testMode:
            webbrowser.open( urls[ client ] + '?dvt=on' )
        else:
            webbrowser.open( urls[ client ] )
    else:
        toggleConsole()
        print( 'Client\'s site not yet specified' )

def openBrowser( self, testMode ):              

    filepath = self.window.active_view().file_name()
    folders = filepath.split( '\\' )
    folder = folders[ len(folders) - 2 ]
    filename = folders[ len(folders) - 1 ]
    
    if filename == 'config.js':
        searchClient( folder.split( '-' )[0], testMode )        
    elif 'observer-compiler' in filepath:
        searchClient( re.search( r'repo\\(.*)\\src', filepath ).group(1), testMode )

class OpenBrowserCommand(sublime_plugin.WindowCommand):
    def run(self):
        openBrowser( self, False )

class OpenBrowserTestModeCommand(sublime_plugin.WindowCommand):
    def run(self):
        openBrowser( self, True )